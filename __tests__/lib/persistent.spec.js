import expect from 'expect.js'
import PersistentCollection from '../../app/lib/persistent'

var call = null
jest.setMock('react-native', {
  AsyncStorage: {
    getItem: (key) => {
      call = {key}
      return new Promise((r) => r('[{"id":1,"name":"test"}]'))
    },
    setItem: (key, val) => {
      call = {key, val}
      return new Promise((r) => r())
    }
  }
})

describe('Persistent Collection', () => {
  beforeEach(() => {
    call = null
  })

  it('should return all items', (done) => {
    const coll = new PersistentCollection('sample')
    coll.all().then(items => {
      expect(items).to.eql([{id:1, name:'test'}])
      done()
    })
    expect(call.key).to.be('sample')
  })

  it('should return all items with afterLoad', (done) => {
    const coll = new PersistentCollection('sample', {
        afterLoad: x => { return { item: x } }
    })
    coll.all().then(items => {
      expect(items).to.eql([{item: {id:1, name:'test'}}])
      done()
    })
    expect(call.key).to.be('sample')
  })

  it('should reset items', (done) => {
    const coll = new PersistentCollection('sample')
    coll.reset([{reset:true}]).then(() => {
      expect(call.key).to.be('sample')
      expect(call.val).to.be('[{"reset":true}]')
      done()
    })
  })

  it('should reset items with beforeSave', (done) => {
    const coll = new PersistentCollection('sample', {
        beforeSave: x => { return {item:x} }
    })
    coll.reset([{reset:true}]).then(() => {
      expect(call.key).to.be('sample')
      expect(call.val).to.be('[{"item":{"reset":true}}]')
      done()
    })
  })

  it('should add new item', (done) => {
    const coll = new PersistentCollection('sample')
    coll.add({id: 2, name: 'hi'}).then(() => {
      expect(call.key).to.be('sample')
      expect(call.val).to.be('[{"id":1,"name":"test"},{"id":2,"name":"hi"}]')
      done()
    })
  })

  it('should update item', (done) => {
    const coll = new PersistentCollection('sample')
    coll.update((x => x.id == 1), {id: 2, name: 'updated'}).then(() => {
      expect(call.key).to.be('sample')
      expect(call.val).to.be('[{"id":2,"name":"updated"}]')
      done()
    })
  })

  it('should remove item', (done) => {
    const coll = new PersistentCollection('sample')
    coll.remove((x => x.id == 1)).then(() => {
      expect(call.key).to.be('sample')
      expect(call.val).to.be('[]')
      done()
    })
  })
})
