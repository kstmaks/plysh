import expect from 'expect.js'
import AppState from '../app/app_state'

describe('app state', () => {
  it('should contain all reducers', () => {
    expect(AppState.getState()).to.have.keys(['routes', 'tasks'])
  })
})
