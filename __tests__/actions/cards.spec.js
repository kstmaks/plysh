import expect from 'expect.js'
import helper from '../helper'
import cardsActions from '../../app/actions/cards'

global.fetch = helper.mockFetch([
  { method: 'post', url: 'https://api.stripe.com/v1/customers', result: { id: 'customerID' } },
  { method: 'delete', url: 'https://api.stripe.com/v1/customers/customerID', result: { } }
])

describe('cards actions', () => {

  it('should contain all actions', () => {
    expect(cardsActions(() => null)).to.have.keys('create', 'remove')
  })

  it('should create card', (done) => {
    const actions = cardsActions((result) => {
      expect(result).to.eql({
          type: 'CARDS_ADD',
          item: {
            title: 'hello, world',
            stripeCustomerId: 'customerID',
            number: '***4321'
          }
      })
    })
    actions.create('hello, world', {number: '1234567887654321'}).then(res => {
        expect(res.success).to.be(true)
        done()
    })
  })

  it('should delete card', (done) => {
    const card = { number: '***4321', stripeCustomerId: 'customerID', title: 'hello, world' }

    const actions = cardsActions((result) => {
      expect(result).to.eql({
          type: 'CARDS_REMOVE',
          item: card
      })
      done()
    })
    actions.remove(card).then(res => {
        expect(res).to.be(true)
        done()
    })
  })

  it('should have store and load functions', () => {
    const actions = cardsActions(x => x)
    expect(actions).to.have.keys('store', 'load')
  })
})
