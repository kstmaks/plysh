import expect from 'expect.js'
import { 
  actionTypeBuilder, 
  collectionActions,
} from '../../app/actions/collection'

var call = null
jest.setMock('react-native', { })

import {AsyncStorage} from 'react-native'

describe('collection actions', () => {
  describe('#actionTypeBuilder', () => {
    it('should prefix title', () => {
      let builder = actionTypeBuilder('sample')
      expect(builder('TEST')).to.be('SAMPLE_TEST')
    })
  })

  describe('#collectionActions', () => {
    const actions = collectionActions('sample')
    const customizedActions = collectionActions('sample', {autoId: true, id: 'sampleId'})

    it('should contain add, update, remove and reset', () => {
      const result = actions(x => x)
      expect(result).to.only.have.keys(['add', 'update', 'remove', 'reset'])
    })

    it('should create ADD action', (done) => {
      const props = actions(res => {
        expect(res).to.be.eql({
            type: 'SAMPLE_ADD',
            item: 'data'
        })
        done()
      })
      props.add('data')
    })

    it('should return promise for ADD', () => {
      const props = actions(x => x)
      const res = props.add('data')
      expect(res).to.be.a(Promise)
    })

    it('should create UPDATE action', (done) => {
      const props = actions(res => {
        expect(res.type).to.be('SAMPLE_UPDATE')
        expect(res.item).to.eql({id: 1, index: 2})
        expect(res.selector({id:1})).to.be(true)
        expect(res.selector({id:7})).to.be(false)
        done()
      })
      props.update({id: 1, index: 2})
    })

    it('should create UPDATE action with custom id', (done) => {
      const props = customizedActions(res => {
        expect(res.type).to.be('SAMPLE_UPDATE')
        expect(res.item).to.eql({sampleId: 1, index: 2})
        expect(res.selector({sampleId:1})).to.be(true)
        expect(res.selector({sampleId:7})).to.be(false)
        done()
      })
      props.update({sampleId: 1, index: 2})
    })

    it('should return promise for UPDATE', () => {
      const props = actions(x => x)
      const res = props.update('data')
      expect(res).to.be.a(Promise)
    })

    it('should create REMOVE action', (done) => {
      const props = actions(res => {
        expect(res.type).to.be('SAMPLE_REMOVE')
        expect(res.selector({id:1})).to.be(true)
        expect(res.selector({id:7})).to.be(false)
        done()
      })
      props.remove({id: 1, index: 2})
    })

    it('should create REMOVE action with custom id', (done) => {
      const props = customizedActions(res => {
        expect(res.type).to.be('SAMPLE_REMOVE')
        expect(res.selector({sampleId:1})).to.be(true)
        expect(res.selector({sampleId:7})).to.be(false)
        done()
      })
      props.remove({sampleId: 1, index: 2})
    })

    it('should return promise for REMOVE', () => {
      const props = actions(x => x)
      const res = props.update('data')
      expect(res).to.be.a(Promise)
    })

    it('should create RESET action', (done) => {
      const props = actions(res => {
        expect(res.type).to.be('SAMPLE_RESET')
        expect(res.items).to.eql([{elem: 1}, {elem: 2}])
        done()
      })
      props.reset([{elem: 1}, {elem: 2}])
    })

    it('should return promise for RESET', () => {
      const props = actions(x => x)
      const res = props.reset([])
      expect(res).to.be.a(Promise)
    })
  })
})
