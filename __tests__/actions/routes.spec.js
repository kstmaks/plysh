import expect from 'expect.js'
import actions from '../../app/actions/routes'

describe('routes actions', () => {

  var dispatchCalled = false
  let dispatch = x => {
    dispatchCalled = true
    return x
  }

  beforeEach(() => {
    dispatchCalled = false
  })

  it('should contain all actions', () => {
    let result = actions(dispatch)
    expect(result).to.have.keys(['push', 'pop', 'replace', 'setParams'])
  })

  it('should create PUSH action', () => {
    let result = actions(dispatch)
    expect(result.push('route')).to.be.eql({
        type: 'ROUTES_PUSH',
        route: 'route',
        params: undefined
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create PUSH action with params', () => {
    let result = actions(dispatch)
    expect(result.push('route', {content:1})).to.be.eql({
        type: 'ROUTES_PUSH',
        route: 'route',
        params: {content:1}
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create POP action', () => {
    let result = actions(dispatch)
    expect(result.pop()).to.be.eql({
        type: 'ROUTES_POP'
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create REPLACE action', () => {
    let result = actions(dispatch)
    expect(result.replace('route')).to.be.eql({
        type: 'ROUTES_REPLACE',
        route: 'route',
        params: undefined
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create REPLACE action with params', () => {
    let result = actions(dispatch)
    expect(result.replace('route', {content:2})).to.be.eql({
        type: 'ROUTES_REPLACE',
        route: 'route',
        params: {content:2}
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create SET_PARAMS action', () => {
    let result = actions(dispatch)
    expect(result.setParams(123)).to.be.eql({
        type: 'ROUTES_SET_PARAMS',
        params: 123
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create RESET_STACK action', () => {
    let result = actions(dispatch)
    expect(result.resetStack([1, 2, 3])).to.be.eql({
        type: 'ROUTES_RESET_STACK',
        stack: [1, 2, 3]
    })
    expect(dispatchCalled).to.be(true)
  })

  it('should create PUSH_MODAL action', () => {
    const props = actions(res => {
      expect(res).to.be.eql({
          type: 'ROUTES_PUSH_MODAL',
          route: 'route',
          params: {param:1}
      })
    })
    props.pushModal('route', {param:1})
  })

  it('should create POP action with result', () => {
    const props = actions(res => {
      expect(res).to.be.eql({
          type: 'ROUTES_POP_MODAL',
          result: {param:1}
      })
    })
    props.popModal({param:1})
  })
})
