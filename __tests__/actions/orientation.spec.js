import expect from 'expect.js'
import actions from '../../app/actions/orientation'

describe('oriantation actions', () => {
  var dispatchCalled = false
  let dispatch = x => {
    dispatchCalled = true
    return x
  }

  beforeEach(() => {
    dispatchCalled = false
  })

  it('should contain add, update and remove', () => {
    let result = actions(dispatch)
    expect(result).to.only.have.keys(['set'])
  })

  it('should create SET action', (done) => {
    const props = actions(result => {
      expect(result).to.be.eql({
          type: 'ORIENTATION_SET',
          orientation: 'LANDSCAPE',
          width: 2,
          height: 4
      })
      done()
    })
    props.set({
        orientation: 'LANDSCAPE',
        width: 2,
        height: 4
    })
  })
})
