import expect from 'expect.js'
import _ from 'underscore'
import persistentCollectionActions from '../../app/actions/persistent_collection'

var call = null
jest.setMock('react-native', {
  AsyncStorage: {
    getItem: (key) => {
      call = {key}
      return new Promise((r) => r('[{"id":1,"name":"test"}]'))
    },
    setItem: (key, val) => {
      call = {key, val}
      return new Promise((r) => r())
    }
  }
})

import {AsyncStorage} from 'react-native'

describe('persistent collection actions', () => {

  const actions = persistentCollectionActions('sample')
  const indexedActions = persistentCollectionActions('sample', { autoId: true })
  const customizedActions = persistentCollectionActions('sample', { id: 'sampleId' })
  const indexedCustomizedActions = persistentCollectionActions('sample', { id: 'sampleId', autoId: true })

  it('should contain load, add, update, remove and reset', () => {
    const result = actions(x => x)
    expect(result).to.only.have.keys(['load', 'add', 'update', 'remove', 'reset'])
  })

  _(['load', 'add', 'update', 'remove', 'reset']).each(act => {
    it(`should return promise for ${act}`, () => {
      const props = actions(x => x)
      const res = props[act]([])
      expect(res).to.be.a(Promise)
    })
  })

  it('should create action for loading', (done) => {
    const result = actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id:1, name:'test'}])
        done()
    })
    result.load()
  })

  it('should create action for adding', (done) => {
    const result = actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id:1, name:'test'}, {name: 'second'}])
        done()
    })
    result.add({name: 'second'})
  })

  it('should create action for adding with autoId', (done) => {
    const result = indexedActions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id:1, name:'test'}, {id: 2, name: 'second'}])
        done()
    })
    result.add({name: 'second'})
  })

  it('should create action for adding with customized id', (done) => {
    const result = customizedActions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id:1, name:'test'}, {name: 'second'}])
        done()
    })
    result.add({name: 'second'})
  })

  it('should create action for adding with indexed customized id', (done) => {
    const result = indexedCustomizedActions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{sampleId: 0, id:1, name:'test'}, {sampleId: 1, name: 'second'}])
        done()
    })
    result.add({name: 'second'})
  })

  it('should create action for updating', (done) => {
    const result = actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id:1, name:'changed'}])
        done()
    })
    result.update({id: 1, name: 'changed'})
  })

  it('should create action for updating with custom id', (done) => {
    const _actions = persistentCollectionActions('sample', { id: 'name' })
    const result = _actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id:7, name:'test'}])
        done()
    })
    result.update({id: 7, name: 'test'})
  })

  it('should create action for removing', (done) => {
    const result = actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([])
        done()
    })
    result.remove({id: 1, name: 'changed'})
  })

  it('should create action for removing with custom id', (done) => {
    const _actions = persistentCollectionActions('sample', { id: 'name' })
    const result = _actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([])
        done()
    })
    result.remove({name: 'test'})
  })

  it('should create action for reseting', (done) => {
    const result = actions(act => {
        expect(act.type).to.eql('SAMPLE_RESET')
        expect(act.items).to.eql([{id: 2, name: 'hey'}])
        done()
    })
    result.reset([{id: 2, name: 'hey'}])
  })
})
