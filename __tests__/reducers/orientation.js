jest.setMock('react-native')

import expect from 'expect.js'
import reducer from '../../app/reducers/routes'

describe('routes reducer', () => {
  it('should initialize with UNKNOWN', () => {
    let res = reducer()
    expect(res).to.eq('UNKNOWN')
  })

  it('should set orientation', () => {
    let res = reducer(reducer(), {
        type: 'ORIENTATION_SET',
        orientation: 'LANDSCALE'
    })
    expect(res).to.eq('LANDSCAPE')
  })
})
