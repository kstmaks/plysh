//
//  AppFrame.m
//  plysh
//
//  Created by Maksim Kostuchenko on 11/29/15.
//

#import "AppFrame.h"
#import "AppDelegate.h"


@implementation AppFrame

@synthesize bridge = _bridge;

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(getSize: (RCTResponseSenderBlock)callback)
{
  CGSize size = [[UIScreen mainScreen] bounds].size;
  callback(@[[NSNumber numberWithFloat:size.width],
             [NSNumber numberWithFloat:size.height]]);
}

@end
