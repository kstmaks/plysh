'use strict';

import React, {
  EdgeInsetsPropType,
  ImageResizeMode,
  ImageStylePropTypes,
  NativeMethodsMixin,
  NativeModules,
  PropTypes,
  ReactNativeViewAttributes,
  StyleSheet,
  StyleSheetPropType,
  flattenStyle,
  invariant,
  merge,
  requireNativeComponent,
  verifyPropTypes,
  warning,
} from 'react-native'

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource'
import _ from 'underscore'

var ExImage = React.createClass({
  getDefaultProps: function() {
    return {
      loadingBackgroundColor: '#E3E3E3',
      loadingForegroundColor: '#F53341',
    };
  },

  statics: {
    resizeMode: ImageResizeMode,
  },

  /*
  mixins: [NativeMethodsMixin],

  /**
   * `NativeMethodsMixin` will look for this when invoking `setNativeProps`. We
   * make `this` look like an actual native component class.
  viewConfig: {
    uiViewClassName: 'UIView',
    validAttributes: ReactNativeViewAttributes.UIView
  },
   */

  render: function() {
    for (var prop in nativeOnlyProps) {
      if (this.props[prop] !== undefined) {
        console.warn('Prop `' + prop + ' = ' + this.props[prop] + '` should ' +
          'not be set directly on Image.');
      }
    }
    var source = resolveAssetSource(this.props.source) || {};

    var {width, height} = source;
    var style = [{width, height}, styles.base, this.props.style];
    // invariant(style, 'style must be initialized');

    var isNetwork = source.uri && source.uri.match(/^https?:/);
    // invariant(
    //   !(isNetwork && source.isStatic),
    //   'static image uris cannot start with "http": "' + source.uri + '"'
    // );
    var isStored = !source.isStatic && !isNetwork;
    var RawImage = isNetwork ? RCTExNetworkImage : RCTExStaticImage;

    if (this.props.style && this.props.style.tintColor) {
      warning(RawImage === RCTExStaticImage, 'tintColor style only supported on static images.');
    }
    var resizeMode = this.props.resizeMode || style.resizeMode || 'cover';

    var nativeProps = _.extend({}, this.props, {
      style,
      tintColor: style.tintColor,
      resizeMode: resizeMode,
    });
    if (nativeProps.cacheThumbnail === undefined) {
      nativeProps.cacheThumbnail = false;
    }
    if (isStored) {
      nativeProps.imageInfo = {
        imageTag: source.uri,
        prezSize: {
          width: style.width || 0,
          height: style.height || 0,
        },
        cacheThumbnail: nativeProps.cacheThumbnail,
      }
    } else {
      nativeProps.src = source.uri;
    }
    if (this.props.defaultSource) {
      nativeProps.defaultImageSrc = this.props.defaultSource.uri;
    }

    nativeProps.onExLoadStart = nativeProps.onLoadStart;
    nativeProps.onExLoadProgress = nativeProps.onLoadProgress;
    nativeProps.onExLoadError = nativeProps.onLoadError;
    nativeProps.onExLoaded = nativeProps.onLoaded;
    delete nativeProps.onLoadStart;
    delete nativeProps.onLoadProgress;
    delete nativeProps.onLoadError;
    delete nativeProps.onLoaded;
    return <RawImage {...nativeProps} />;
  }
});

var styles = StyleSheet.create({
  base: {
    overflow: 'hidden',
    backgroundColor: '#EFEFEF',
  },
});

var RCTExNetworkImage = requireNativeComponent('RCTExNetworkImage', null);
var RCTExStaticImage = requireNativeComponent('RCTExStaticImage', null);

var nativeOnlyProps = {
  src: true,
  defaultImageSrc: true,
  imageTag: true,
  contentMode: true,
  imageInfo: true,
};
// if (__DEV__) {
//   verifyPropTypes(ExImage, RCTExStaticImage.viewConfig, nativeOnlyProps);
//   verifyPropTypes(ExImage, RCTExNetworkImage.viewConfig, nativeOnlyProps);
// }

ExImage.calculateCacheSize = function(callback) {
  NativeModules.ExNetworkImageManager.calculateCacheSize(callback);
}

ExImage.clearCache = function(callback) {
  NativeModules.ExNetworkImageManager.clearCache(callback);
}

ExImage.clearThumbnailCache = function(callback) {
  NativeModules.RCTExStaticImageManager.clearThumbnailCache(callback);
}

module.exports = ExImage;
