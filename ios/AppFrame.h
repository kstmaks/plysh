//
//  AppFrame.h
//  plysh
//
//  Created by Maksim Kostuchenko on 11/29/15.
//

#ifndef AppFrame_h
#define AppFrame_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RCTBridgeModule.h"
#import "RCTBridge.h"
#import "RCTEventDispatcher.h"
#import "AppDelegate.h"

@interface AppFrame : NSObject <RCTBridgeModule>
@end

@interface AppDelegate (AppFrame)

@end

#endif /* AppFrame_h */
