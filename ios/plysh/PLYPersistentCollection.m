//
//  PLYPersistentCollection.m
//  plysh
//
//  Created by Maksim Kostuchenko on 1/13/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "PLYPersistentCollection.h"
#import "RCTAsyncLocalStorage.h"
#import "RCTConvert.h"



@implementation PLYPersistentCollection

- (id)init {
  id obj = [super init];
  storage = [[RCTAsyncLocalStorage alloc] init];
  collection = @[];
  return obj;
}

- (id)initWithTitle:(NSString*)_title {
  id obj = [self init];
  title = _title;
  return obj;
}

- (void)loadWithCallback:(void (^)(PLYPersistentCollection*collection))callback {
  dispatch_async([storage methodQueue], ^{
    [storage _ensureSetup];
    
    NSDictionary *errorOut = [[NSDictionary alloc] init];
    
    NSString *data = [storage _getValueForKey:title errorOut:&errorOut];
    
    [self parseJSON:data];
    
    dispatch_async(dispatch_get_main_queue(), ^{
      callback(self);
    });
  });
}

- (void)saveWithCallback:(void (^)(PLYPersistentCollection *))callback {
  dispatch_async([storage methodQueue], ^{
    [storage _ensureSetup];
    NSArray *data = @[title, [self toJSON]];
    BOOL changedManifest;
    
    [storage _writeEntry:data changedManifest:&changedManifest];
    
    dispatch_async(dispatch_get_main_queue(), ^{
      callback(self);
    });
  });
}

- (NSString*)toJSON {
  NSError *jsonError;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:collection options:kNilOptions error:&jsonError];
  return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (void)parseJSON:(NSString*)json {
  NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
  NSError *jsonError;

  collection = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
}

- (NSArray*)collection {
  return collection;
}

- (NSObject*)findWithPred:(BOOL (^)(NSObject*obj))pred {
  for (NSObject *obj in collection) {
    if (pred(obj)) return obj;
  }
  return [NSNull null];
}

- (void)mapCollection:(NSObject *(^)(NSObject *))pred {
  NSMutableArray *array = [[NSMutableArray alloc] init];
  for (NSObject *obj in collection) {
    NSObject *el = pred(obj);
    [array addObject:el];
  }
  collection = array;
}

+ (NSDate*)parseDate:(NSString *)source {
  NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
  [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
  return [dateFormat dateFromString:source];
}

@end
