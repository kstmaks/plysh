//
//  PLYPersistentCollection.m
//  plysh
//
//  Created by Maksim Kostuchenko on 1/13/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import "PLYPersistentObject.h"
#import "PLYPersistentCollection.h"
#import "RCTAsyncLocalStorage.h"
#import "RCTConvert.h"

@implementation PLYPersistentObject

- (id)init {
  id obj = [super init];
  storage = [[RCTAsyncLocalStorage alloc] init];
  object = @{};
  return obj;
}

- (id)initWithTitle:(NSString*)_title {
  id obj = [self init];
  title = _title;
  return obj;
}

- (void)loadWithCallback:(void (^)(PLYPersistentObject*object))callback {
  
  dispatch_async([storage methodQueue], ^{
    [storage _ensureSetup];
    
    NSDictionary *errorOut = [[NSDictionary alloc] init];
    
    NSString *data = [storage _getValueForKey:title errorOut:&errorOut];
    
    if (data != nil) {
      [self parseJSON:data];
      
      dispatch_async(dispatch_get_main_queue(), ^{
        callback(self);
      });
    }
  });
}

- (void)saveWithCallback:(void (^)(PLYPersistentObject *))callback {
  dispatch_async([storage methodQueue], ^{
    [storage _ensureSetup];
    NSArray *data = @[title, [self toJSON]];
    BOOL changedManifest;
    
    [storage _writeEntry:data changedManifest:&changedManifest];
    
    dispatch_async(dispatch_get_main_queue(), ^{
      callback(self);
    });
  });
}

- (NSString*)toJSON {
  NSError *jsonError;
  NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:kNilOptions error:&jsonError];
  return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (void)parseJSON:(NSString*)json {
  NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
  NSError *jsonError;
  
  object = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
}

- (NSDictionary*)object {
  return object;
}

- (void)setObject:(NSDictionary*)o {
  self.object = o;
}

- (id)objectForKey:(id)key {
  return [object objectForKey:key];
}

@end
