//
//  PLYPersistentCollection.h
//  plysh
//
//  Created by Maksim Kostuchenko on 1/13/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTAsyncLocalStorage.h"
#import "RCTAsyncLocalStorage+PrivateAccess.h"

@interface PLYPersistentObject : NSObject {
  NSString *title;
  RCTAsyncLocalStorage *storage;
  NSDictionary *object;
}

- (id)initWithTitle:(NSString*)title;

- (void)loadWithCallback:(void (^)(PLYPersistentObject*collection))callback;
- (void)saveWithCallback:(void (^)(PLYPersistentObject*collection))callback;

- (NSDictionary*)object;
- (void)setObject:(NSDictionary*)object;
- (id)objectForKey:(id)key;

@end
