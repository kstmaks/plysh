//
//  PLYPersistentCollection.h
//  plysh
//
//  Created by Maksim Kostuchenko on 1/13/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCTAsyncLocalStorage.h"
#import "RCTAsyncLocalStorage+PrivateAccess.h"

#define IS_NSNULL(expr) ((id)(expr) == (id)[NSNull null])


@interface PLYPersistentCollection : NSObject {
    NSString *title;
    RCTAsyncLocalStorage *storage;
    NSArray *collection;
}

- (id)initWithTitle:(NSString*)title;

- (void)loadWithCallback:(void (^)(PLYPersistentCollection*collection))callback;
- (void)saveWithCallback:(void (^)(PLYPersistentCollection*collection))callback;

- (NSArray*)collection;
- (NSObject*)findWithPred:(BOOL (^)(NSObject*obj))pred;
- (void)mapCollection:(NSObject* (^)(NSObject*obj))pred;

+ (NSDate*)parseDate:(NSString*)source;

@end
