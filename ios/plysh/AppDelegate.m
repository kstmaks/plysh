/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import "RCTRootView.h"
#import "RCTBridge.h"
#import "RCTEventDispatcher.h"
#import "PLYPersistentCollection.h"
#import "PLYPersistentObject.h"
#import "RCTDevLoadingView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <asl.h>
#import "RCTLog.h"

#define MIXPANEL
#ifdef MIXPANEL
#import "Mixpanel.h"
#define MIXPANEL_TOKEN @"852fc153923fa521458f0b61e0f21b83"
#endif

//#define PREVIEW

@implementation AppDelegate

- (NSURL*)bundleUrl
{
#ifdef PREVIEW
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#else
  NSString *debugHost = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DebugHost"];
  NSString *urlString = [NSString stringWithFormat:@"http://%@:8081/index.ios.bundle?platform=ios&dev=true", debugHost];
  return [NSURL URLWithString:urlString];
#endif
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  [Fabric with:@[[Crashlytics class]]];
#ifdef MIXPANEL
  [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
#endif
  RCTSetLogThreshold(RCTLogLevelInfo);
  RCTSetLogFunction(CrashlyticsReactLogFunction);

  // Request permissions for notifications
  if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
  }

  // Schedule background fetcher
  [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];

  // Initialize facebook sdk
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];


  // Load bundle
  [RCTDevLoadingView setEnabled:NO];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:[self bundleUrl]
                                                      moduleName:@"plysh"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];



  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [[UIViewController alloc] init];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];

  return YES;
}

// Tweak for facebook sdk
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
  return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                        openURL:url
                                              sourceApplication:sourceApplication
                                                     annotation:annotation
          ];
}

// Background event
- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
  NSLog(@"Background fetch!");


  PLYPersistentObject *settings = [[PLYPersistentObject alloc] initWithTitle:@"settings"];

  [settings loadWithCallback:^(PLYPersistentObject *settings) {
    PLYPersistentCollection *collection = [[PLYPersistentCollection alloc] initWithTitle: @"tasks"];

    [collection loadWithCallback:^(PLYPersistentCollection *collection) {

      NSNumber *notificationTime = [settings objectForKey:@"notificationTime"];
      NSTimeInterval hoursThreshold = notificationTime == nil ? 24*7 : [notificationTime doubleValue];

      NSTimeInterval threshold = 60 * 60 * hoursThreshold;
      NSTimeInterval notificationThreshold = 60 * 60 * 24 * 1;

      __block int count = 0;
      __block NSString *title = @"";
      __block NSTimeInterval failTime = threshold;
      __block BOOL sendNotification = false;

      [collection mapCollection:^NSObject *(NSObject *obj) {
        NSDictionary *dict = (NSDictionary *)obj;
        NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:dict];

        BOOL archived = [[dict objectForKey:@"archived"] boolValue];
        NSDate *dateTo = [PLYPersistentCollection parseDate:[dict objectForKey:@"dateTo"]];
        NSTimeInterval timeUntilFail = [dateTo timeIntervalSinceNow];
        NSString *lastNotificationSrc = [dict objectForKey:@"lastNotification"];

        BOOL notificationAllowed = IS_NSNULL(lastNotificationSrc);

        if (!IS_NSNULL(lastNotificationSrc)) {
          NSNumber *lastNotification = (NSNumber*) lastNotificationSrc;
          NSDate *lastNotificationDate = [NSDate dateWithTimeIntervalSince1970:[lastNotification doubleValue]];
          NSTimeInterval notificationDiff = [lastNotificationDate timeIntervalSinceNow];
          notificationAllowed = ABS(notificationDiff) >= notificationThreshold;
        }

        NSLog(@"Task '%@' %@ is %f", [dict objectForKey:@"title"], (archived ? @"true" : @"false"), timeUntilFail);

        if (!archived && timeUntilFail > 0 && timeUntilFail < threshold) {

          count += 1;

          if (notificationAllowed && !sendNotification) {
            sendNotification = true;
            title = [dict objectForKey:@"title"];
            failTime = timeUntilFail;
            [result setObject:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] forKey:@"lastNotification"];
          }
        }

        return result;
      }];

      [collection saveWithCallback:^(PLYPersistentCollection *collection) {
        NSLog(@"Setting count to %d", count);
        application.applicationIconBadgeNumber = count;
        if (sendNotification) {
          [self sendFailNotification:failTime for:title];
        }
        completionHandler(UIBackgroundFetchResultNewData);
      }];
    }];

  }];
}

- (void)sendFailNotification:(NSTimeInterval)timeUntilFail for:(NSString*)title {
  UILocalNotification *localNotif = [[UILocalNotification alloc] init];

  double hours = timeUntilFail / 60 / 60;
  double days = hours / 24;

  NSString *message = (days > 1) ?
  [NSString stringWithFormat:@"Your task %@ will expire in less than %d days!", title, (int)round(days)] :
  [NSString stringWithFormat:@"Your task %@ will expire in less than %d hours!", title, (int)round(hours)];

  if (localNotif) {
    localNotif.alertBody = message;
    localNotif.alertAction = @"OK";
    localNotif.soundName = UILocalNotificationDefaultSoundName;

    [[NSUserDefaults standardUserDefaults] synchronize];
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
  }
}



RCTLogFunction CrashlyticsReactLogFunction = ^(
                                               RCTLogLevel level,
                                               RCTLogSource source,
                                               NSString *fileName,
                                               NSNumber *lineNumber,
                                               NSString *message
                                               )
{
  NSString *log = RCTFormatLog([NSDate date], level, fileName, lineNumber, message);
  CLS_LOG(@"REACT LOG: %s", log.UTF8String);

#ifdef DEBUG
  fprintf(stderr, "%s\n", log.UTF8String);
  fflush(stderr);
#else
#ifdef PREVIEW
#ifdef MIXPANEL
  Mixpanel *mixpanel = [Mixpanel sharedInstance];
  [mixpanel track:@"crash!" properties:@{ @"log": log }];
#endif
#endif
#endif

  int aslLevel;
  switch(level) {
    case RCTLogLevelInfo:
      aslLevel = ASL_LEVEL_NOTICE;
      break;
    case RCTLogLevelWarning:
      aslLevel = ASL_LEVEL_WARNING;
      break;
    case RCTLogLevelError:
      aslLevel = ASL_LEVEL_ERR;
      break;
    default:
      aslLevel = ASL_LEVEL_CRIT;
      break;
  }
  asl_log(NULL, NULL, aslLevel, "%s", message.UTF8String);
};

@end
