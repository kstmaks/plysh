//
//  RCTAsyncLocalStorage+PrivateAccess.h
//  plysh
//
//  Created by Maksim Kostuchenko on 5/19/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#ifndef RCTAsyncLocalStorage_PrivateAccess_h
#define RCTAsyncLocalStorage_PrivateAccess_h

@protocol PrivateAccess <NSObject>
@end

@interface RCTAsyncLocalStorage (PrivateAccess)

- (NSString *)_getValueForKey:(NSString *)key errorOut:(NSDictionary **)errorOut;
- (NSDictionary *)_ensureSetup;
- (NSDictionary *)_writeEntry:(NSArray<NSString *> *)entry changedManifest:(BOOL *)changedManifest;

@end

#endif /* RCTAsyncLocalStorage_PrivateAccess_h */
