//
//  PLYImageManager.m
//  plysh
//
//  Created by Maksim Kostuchenko on 2/25/16.
//  Copyright © 2016 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLYImageManager.h"

#import <UIKit/UIKit.h>

#import "RCTConvert.h"
#import "RCTBridge.h"
//#import "RCTExStaticImage.h"
//#import "RCTExImageLoader.h"
#import "RCTUIManager.h"

#import "RCTBridgeModule.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <UIKit/UIKit.h>

@implementation PLYImageManager

RCT_EXPORT_MODULE()


RCT_EXPORT_METHOD(getSize:(nonnull NSNumber*)reactTag callback:(RCTResponseSenderBlock)callback) {
  /*[self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary *viewRegistry) {
    UIImageView* view = viewRegistry[reactTag];
    
    int width = 0;
    int height = 0;
    
    if ([view isKindOfClass:[RCTExStaticImage class]]) {
      width = [[view image] size].width;
      height = [[view image] size].height;
    } else {
      width = 0;
      height = 0;
    }
    
    NSNumber *w = [[NSNumber alloc] initWithInt:width];
    NSNumber *h = [[NSNumber alloc] initWithInt:height];
    callback(@[@{@"width": w, @"height": h}]);
  }];*/
}

RCT_EXPORT_METHOD(temporarySave:(NSDictionary*)info callback:(RCTResponseSenderBlock)callback) {
  NSString *tag = info[@"tag"];
  
  if ([tag hasPrefix:@"http"]) {
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:tag]]];
    [PLYImageManager _temporarySave:image callback:callback];
  } else {
    
    // Create NSURL from uri
    NSURL *url = [[NSURL alloc] initWithString:tag];
    
    // Create an ALAssetsLibrary instance. This provides access to the
    // videos and photos that are under the control of the Photos application.
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Using the ALAssetsLibrary instance and our NSURL object open the image.
    [library assetForURL:url resultBlock:^(ALAsset *asset) {
      
      // Create an ALAssetRepresentation object using our asset
      // and turn it into a bitmap using the CGImageRef opaque type.
      ALAssetRepresentation *representation = [asset defaultRepresentation];
      CGImageRef imageRef = [representation fullResolutionImage];
      
      // Create UIImageJPEGRepresentation from CGImageRef
      UIImage *image = [UIImage imageWithCGImage:imageRef];
      
      
      [PLYImageManager _temporarySave:image callback:callback];
      
      
    } failureBlock:^(NSError *error) {
      NSLog(@"that didn't work %@", error);
    }];
  }
}

+ (void)_temporarySave:(UIImage*)image callback:(RCTResponseSenderBlock)callback {
  NSData *data = [NSData dataWithData:UIImageJPEGRepresentation(image, 0.8f)];
  NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
  NSURL *fileURL = [[tmpDirURL URLByAppendingPathComponent:@"pkm"] URLByAppendingPathExtension:@"jpg"];
  NSString *path = [fileURL path];
  NSLog(@"fileURL: %@", path);
  [data writeToFile:path atomically:YES];
  callback(@[@{@"path":path}]);
}

@end