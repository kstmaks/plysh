import _ from 'underscore'
import PersistentCollection from '../lib/persistent'
import {actionTypeBuilder, collectionActions} from './collection'

export default function persistentCollectionActions(title, opts={}) {
  const type = actionTypeBuilder(title)
  const id = opts.id || "id"
  const autoId = !!opts.autoId
  const afterLoad = opts.afterLoad || (x => x)
  const beforeSave = opts.beforeSave || (x => x)

  const runtimeActions = collectionActions(title, opts)
  const collection = new PersistentCollection(title, {
      afterLoad, beforeSave
  })

  return (dispatch) => {
    function dispatchReset(items) {
      return dispatch({
          type: type('RESET'),
          items
      })
    }

    function formatWithId(items) {
      const max = _(items).max(x => x[id])[id]
      var maxId = max ? max+1 : 0
      return _(items).map((item, i) => {
        if (!autoId || item[id]) {
          return item
        } else {
          var data = {}
          data[id] = maxId++
          return _.extend({}, item, data)
        }
      })
    }

    function update(items, item) {
      return _(items).map(i => {
        if (i[id] == item[id]) {
          return item
        } else {
          return i
        }
      })
    }

    return {
      load: () => collection.all()
        .then(items => formatWithId(items))
        .then(items => dispatchReset(items)),

      add: (item) => collection.all()
        .then(items => items.concat([item]))
        .then(items => formatWithId(items))
        .then(items => {
          return collection.reset(items).then(() => dispatchReset(items))
        }),

      update: (item) => collection.all()
        .then(items => formatWithId(items))
        .then(items => update(items, item))
        .then(items => {
          return collection.reset(items).then(() => dispatchReset(items))
        }),

      remove: (item) => collection.all()
        .then(items => formatWithId(items))
        .then(items => _(items).filter(i => i[id] != item[id]))
        .then(items => {
          return collection.reset(items).then(() => dispatchReset(items))
        }),

      reset: (items) => collection.reset(formatWithId(items))
        .then(() => dispatchReset(items)),

      runtime: runtimeActions(dispatch)
    }
  }
}

