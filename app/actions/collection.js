export function actionTypeBuilder(title) {
  return (type) => title.toUpperCase() + '_' + type
}

export function collectionActions(title, opts={}) {
  const type = actionTypeBuilder(title)
  const id = opts.id || "id"
  const autoId = !!opts.autoId

  const promise = (param) => new Promise(r => r(param))

  return (dispatch) => {
    return {
      add: (item) => {
        dispatch({
            type: type('ADD'),
            item
        })
        return promise(item)
      },

      update: (item) => {
        dispatch({
            type: type('UPDATE'),
            selector: x => x[id] == item[id],
            item,
        })
        return promise(item)
      },

      remove: (item) => {
        dispatch({
            type: type('REMOVE'),
            selector: x => x[id] == item[id]
        })
        return promise(item)
      },

      reset: (items) => {
        dispatch({
            type: type('RESET'),
            items
        })
        return promise(items)
      }
    }
  }
}
