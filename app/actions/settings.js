import {AsyncStorage} from 'react-native'
import _ from 'underscore'

export default function settingsActions(dispatch) {
  const setMultiple = (values) => {
    return dispatch((dispatch, getState) => {
      const state = getState()
      const changed = _.extend({}, state.settings, values)

      return AsyncStorage
        .setItem('settings', JSON.stringify(changed))
        .then(() => dispatch({
              type: 'SETTINGS_SET',
              settings: changed
        }))
    })
  }

  return {
    load: () =>
      AsyncStorage.getItem('settings')
        .then(raw => JSON.parse(raw || '{}'))
        .then(json => dispatch({
              type: 'SETTINGS_SET',
              settings: json
        })),

    set: (field, value) => {
      if (typeof(field) == 'string') {
        return setMultiple({[field]: value})
      } else {
        return setMultiple(field)
      }
    }
  }
}
