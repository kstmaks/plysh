import moment from 'moment'
import collectionReducer from './collection'

export default collectionReducer('recipients', {
    initialState: [
      { 
        id: 'wwf',
        type: 'charity', 
        title: 'WWF', 
        image: require('../images/wwf.jpg')
      },
      { 
        id: 'redcross',
        type: 'charity', 
        title: 'Red Cross', 
        image: require('../images/redcross.jpg')
      },
      { 
        id: 'feedchildren',
        type: 'charity', 
        title: 'Feed the Children', 
        image: require('../images/feedchildren.png')
      },
      { 
        id: 'salvationarmy',
        type: 'charity', 
        title: 'The Salvation Army', 
        image: require('../images/salvationarmy.jpg')
      },
      { 
        id: 'metropolitanmuseumofart',
        type: 'charity', 
        title: 'Metropolitan Museum of Art', 
        image: require('../images/metropolitanmuseumofart.jpg')
      },

      { 
        id: 'drake',
        type: 'celebrity', 
        title: 'Drake', 
        image: require('../images/drake.jpg')
      },
      { 
        id: 'rickross',
        type: 'celebrity', 
        title: 'Rick Ross', 
        image: require('../images/rickross.png')
      },
      { 
        id: 'rihanna',
        type: 'celebrity', 
        title: 'Rihanna', 
        image: require('../images/rihanna.jpg')
      },
      { 
        id: 'markzuckerberg',
        type: 'celebrity', 
        title: 'Mark Zuckerberg', 
        image: require('../images/markzuckerberg.jpeg')
      },
      { 
        id: 'kimkardashian',
        type: 'celebrity', 
        title: 'Kim Kardashian', 
        image: require('../images/kimkardashian.jpg')
      },
    ]
});
