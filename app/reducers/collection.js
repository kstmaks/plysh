import { actionTypeBuilder } from '../actions/collection'
import _ from 'underscore'

// (string, object) -> (coll, action) -> coll
export default function collectionReducer(title, opts={}) {
  const extendedHandler = opts.extendedHandler || (x => x)
  const initialState = opts.initialState || []
  const type = actionTypeBuilder(title)

  let reducer = (source, action) => {
    const state = source || initialState
    const item = action.item

    switch (action.type) {

    case type('ADD'):
      return state.concat([item])

    case type('UPDATE'):
      return state.map((old) => (action.selector(old)) ? action.item : old) 

    case type('REMOVE'):
      return state.filter(x => !action.selector(x))

    case type('RESET'):
      return action.items

    default:
      return extendedHandler(state, action, type)
    }
  }

  return reducer
}
