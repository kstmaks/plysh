import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  dot: {
    width: 8,
    height: 8,
    backgroundColor: 'rgb(155,155,155)',
    borderRadius: 4,
    marginRight: 10,
    marginLeft: 10,
  },
  dotActive: {
    backgroundColor: 'rgb(200,39,84)',
  },
  tabBar: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-end',
    flexDirection: 'row',
    backgroundColor: 'white'
  },
})
