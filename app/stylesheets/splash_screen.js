import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  back: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  text: {
    fontSize: 40,
    color: 'white',
    textAlign: 'center',
    fontWeight: "200",
    marginBottom: 10,
  }
})
