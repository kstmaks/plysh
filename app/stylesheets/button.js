import { StyleSheet } from 'react-native'

const height = 50

export default StyleSheet.create({
  primaryButtonWrapper: {
    height: height,
    backgroundColor: 'rgb(200,39,84)',
  },
  primaryButton: {
    height: height,
    backgroundColor: 'rgb(250,69,114)',
    justifyContent: 'center',
  },
  primaryButtonText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white'
  },

  grayButton: {
    backgroundColor: 'rgb(199,199,199)',
  },

  applyButton: {
    backgroundColor: 'rgb(103,178,103)',
    margin: 5,
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 12,
    paddingRight: 12,
    borderRadius: 5
  },

  applyButtonText: {
    color: 'white',
    textAlign: 'center'
  }
})
