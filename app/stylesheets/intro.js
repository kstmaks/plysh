import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    color: '#333',
    fontSize: 17,
  },
})
