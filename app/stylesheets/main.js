import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'rgb(29,29,29)'
  },

  bgDefault: {
    backgroundColor: 'rgb(31,31,31)'
  },

  bgTablet: {
    backgroundColor: 'rgb(21,21,21)',
    flex: 1,
    flexDirection: 'row'
  },

  bgLight: {
    backgroundColor: 'white'
  },

  textPrimary: {
    color: 'rgb(228,55,98)'
  },

  tabletLeftMenu: {
    flex: 1,
    shadowColor: 'rgb(0,0,0)',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.4,
    shadowRadius: 10,
  },

  tabletSideMenu: {
    flex: 2,
    borderLeftWidth: 1,
    borderColor: 'rgba(0,0,0,.4)'
  },

  mainBox: {
    flex: 1
  },

  cardsRow: {
    flexDirection: 'row',
    paddingTop: 17,
    paddingBottom: 17,
    paddingLeft: 12,
    paddingRight: 12,
  },

  cardsRowText: {
    flex: 1
  },

  recipientContent: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 17,
    paddingRight: 17,
  },

  recipientText: {
    color: 'white',
    textAlign: 'left',
    fontSize: 20
  },

  tabberBar: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    height: 40
  },

  tabberTab: {
    flex: 1,
    textAlign: 'center',
    color: '#AAA',
    marginBottom: 10
  },

  tabberTabSelected: {
    color: 'rgb(228,55,98)'
  },

  cameraCover: {
    backgroundColor: 'rgba(0,0,0,.7)',
    flex: 1,
  },

  cameraLive: {
    flex: 1,
    backgroundColor: 'transparent'
  },

  cameraCoverText: {
    alignItems: 'center',
  },

  cameraButtonContainer: {
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },

  plyshText: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white',
  },

  cameraButton: {
    width: 70,
    height: 70,
    borderRadius: 35,
    backgroundColor: 'rgb(228,55,98)',
    shadowColor: 'rgb(0,0,0)',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.4,
    shadowRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },

  card: {
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderBottomColor: '#CCC'
  },

  cardTitle: {
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'row'
  },

  cardDescription: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },

  cardTitleText: {
    flex: 1
  },

  cardDescriptionText: {
    flex: 1
  },

  cardTitleDateText: {
    color: '#777'
  },

  cardImageWrapper: {
    shadowRadius: 3,
    shadowColor: '#777',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 1,
    margin: 5,
    alignSelf: 'center'
  },

  cardImage: {
  },

  cardPlyshed: {
    padding: 10,
    paddingBottom: 7,
    color: 'rgb(228,55,98)',
    borderColor: 'rgb(228,55,98)',
    borderWidth: 3,
    fontSize: 17,
    flex: 1,
    textAlign: 'center'
  },

  cardFailed: {
    padding: 10,
    paddingBottom: 7,
    color: '#555',
    borderColor: '#555',
    borderWidth: 3,
    fontSize: 17,
    flex: 1,
    textAlign: 'center'
  },

  confirmFailTitle: {
    color: 'white',
    margin: 15,
    fontSize: 25
  },
  confirmFailTitleTask: {
    color: 'rgb(228,55,98)'
  },
  bill: {
    padding: 15,
    backgroundColor: 'white'
  },

  billDescription: {
    fontSize: 17,
  },
  billTotal: {
    fontSize: 25,
  },
  billCharge: {
    fontWeight: 'bold'
  },
  confirmFailImage: {
    height: 100,
    width: null
  },
  confirmFailAccept: {
    padding: 10,
    margin: 15,
    borderWidth: 1,
    borderColor: 'rgb(228,55,98)',
    borderRadius: 15,
    backgroundColor: 'rgb(228,55,98)',
    marginBottom: 0
  },
  confirmFailAcceptText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white'
  },
  confirmFailReject: {
    padding: 10,
    margin: 15,
    borderWidth: 1,
    borderColor: 'rgba(255,255,255,.4)',
    borderRadius: 15,
  },
  confirmFailRejectText: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white'
  },


  flexCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  loginDateSelector: {
    padding: 5,
    borderRadius: 5,
    backgroundColor: 'white',
  },

  logoText: {
    color: 'rgb(228,55,98)',
    fontSize: 65,
    textAlign: 'center',
    fontWeight: '100',
  },

  loginSubtitle: {
    color: 'white',
    fontSize: 17,
    textAlign: 'center'
  },

  loginInputWrapper: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },

  loginInput: {
    flex: 1,
    height: 50,
    color: 'white',
    padding: 10,
    borderWidth: 0
  },

  loginInputBorder: {
    height: 1,
    alignSelf: 'stretch',
    backgroundColor: 'rgba(255,255,255,0.3)'
  },

  loginError: {
    marginTop: 3,
    color: 'rgb(255, 34, 34)'
  },

  calendarContainer: {
    backgroundColor: 'white'
  },

  calendarSelectedCircle: {
    backgroundColor: 'rgb(228,55,98)',
  },

  calendarButton: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'rgb(228,55,98)',
    borderRadius: 5
  }
})
