import { StyleSheet } from 'react-native'

const height = 100

export default StyleSheet.create({
  listContainer: {
    position: 'relative'
  },
  addButtonWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 100,
    height: 100,
    backgroundColor: 'transparent'
  },
  addButton: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'rgb(228,55,98)',
    shadowColor: 'rgb(0,0,0)',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.4,
    shadowRadius: 10,
  },
  iconStyle: {
    width: 30,
    height: 30,
    margin: 10
  },

  container: {
    position: 'relative',
    height: height,
    backgroundColor: 'white',
    overflow: 'hidden'
  },
  image: {
    width: null,
    height: height
  },
  content: {
    backgroundColor: 'rgba(0,0,0,.7)',
    height: height
  },
  textHolder: {
    flex: 1,
    paddingTop: 10,
    paddingLeft: 15,
    paddingBottom: 10,
    paddingRight: 15,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  titleAndPrice: {
    flex: 1,
    alignSelf: 'stretch'
  },
  title: {
    fontSize: 25,
    flex: 1,
    color: 'white'
  },
  price: {
    flex: 0,
    fontSize: 15
  },
  date: {
    alignSelf: 'center',
    alignItems: 'center'
  },
  dateValue: {
    color: 'white',
    fontSize: 30
  },
  dateMetric: {
    color: 'white',
  },

  detailsFooter: {
    flexDirection: 'row'
  },

  detailsButton: {
    flex: 1
  },

  detailsRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,.1)'
  },
  detailsRowError: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(240,200,200)'
  },

  detailsRowText: {
    flex: 1,
    marginTop: 17,
    marginBottom: 17,
    marginLeft: 12,
    marginRight: 12,
    color: '#555'
  },
  detailsRowTextError: {
    color: '#922'
  },

  detailsRowErrorDescription: {
    backgroundColor: 'rgb(240,200,200)',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 12,
    paddingRight: 12,
    position: 'relative'
  },
  detailsErrorTriangle: {
    position: 'absolute',
    top: -5,
    left: 12,
    backgroundColor: 'rgb(240,200,200)',
    width: 10,
    height: 10,
    transform: [{rotate: '45deg'}]
  },

  detailsRowTextRight: {
    textAlign: 'right',
    flex: 0
  },

  formText: {
    height: 25,
    flex: 1,
    marginTop: 17,
    marginBottom: 17,
    marginLeft: 12,
    marginRight: 12,
  },
  formTextError: {
    color: '#922'
  },

  cover: {
    height: height,
    marginTop: -height,
    backgroundColor: 'rgba(255,0,0,.4)'
  },

  tutorialFade: {
    backgroundColor: 'rgba(0,0,0,.7)',
    position: 'absolute',
    top: 0,
    left: 0,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  tutorialFadeText: {
    fontSize: 30,
    margin: 30,
    color: 'white',
    textAlign: 'center',
  }
})
