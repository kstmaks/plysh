import { compose, applyMiddleware, combineReducers, createStore } from 'redux'
import _ from 'underscore'
import thunk from 'redux-thunk'

import tasks from './reducers/tasks'
import routes from './reducers/routes'
import orientation from './reducers/orientation'
import cards from './reducers/cards'
import recipients from './reducers/recipients'
import plysh from './reducers/plysh'
import settings from './reducers/settings'

const appStateReducer = combineReducers({
    tasks,
    routes,
    orientation,
    cards,
    recipients,
    plysh,
    settings
})

var i = 0
const devtools = store => next => action => {
  const result = next(action)
  if (console.group) {
    console.group(`#${i++}`, action.type, action)
    _(store.getState()).map((val, key) => {
      console.log(key, val)
    })
    console.groupEnd()
  }
  return result
}

const store = compose(
  applyMiddleware(thunk, devtools)
)(createStore)(appStateReducer)

export default store
