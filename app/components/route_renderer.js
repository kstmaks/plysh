import React, { 
  View, 
  Text,
  TouchableHighlight,
  Navigator,
  NavigationBarRouteMapper
} from 'react-native'
import _ from 'underscore'

import connector from '../connector'
import routes from '../actions/routes'

import SplashScreen from './views/splash_screen'
import Intro from './views/intro'
import Tasks from './views/tasks'
import Archive from './views/archive'
import Calendar from './views/calendar'
import TaskDetails from './views/task_details'
import TaskForm from './views/task_form'
import Cards from './views/cards'
import CardForm from './views/card_form'
import CardDetails from './views/card_details'
import Recipients from './views/recipients'
import TaskCamera from './views/task_camera'
import PlyshHistory from './views/plysh_history'
import ConfirmFail from './views/confirm_fail'
import Terms from './views/terms'
import Login from './views/login'
import Settings from './views/settings'

export default class RouteRenderer extends React.Component {
  getRoutes() {
    return {
      splashScreen: SplashScreen,
      intro: Intro,
      tasks: Tasks,
      archive: Archive,
      calendar: Calendar,
      taskDetails: TaskDetails,
      taskForm: TaskForm,
      cards: Cards,
      cardForm: CardForm,
      cardDetails: CardDetails,
      recipient: Recipients,
      taskCamera: TaskCamera,
      plyshHistory: PlyshHistory,
      confirmFail: ConfirmFail,
      terms: Terms,
      login: Login,
      settings: Settings
    }
  }

  render() {
    let route = this.props.route
    let routes = this.getRoutes()

    if (route.name in routes) {
      const [Component, params] = _.flatten([routes[route.name]])
      return <Component {..._.extend({}, route.params, params)} />
    } else {
      return <Text>404</Text>
    }
  }
}
