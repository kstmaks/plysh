import React, {
  View,
  Text,
  ListView,
  TouchableOpacity,
  TouchableHighlight,
  InteractionManager,
} from 'react-native'
import _ from 'underscore'
import moment from 'moment'
import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import tasks from '../../actions/tasks'
import plysh from '../../actions/plysh'
import cards from '../../actions/cards'
import orientation from '../../actions/orientation'
import settings from '../../actions/settings'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'
import Routes from '../../lib/routes'
import Main from './main'
import Money from '../forms/money'
import Sidebar from './sidebar'
import Tabber from '../forms/tabber'
import PlyshHistory from './plysh_history'

import CollectionList from '../forms/collection_list'
import Task from './tasks/task'

@connector({plysh, cards, tasks, routes, orientation, settings})
export default class Tasks extends React.Component {
  state = {
    sidebarOpen: false,
    view: 'tasks' // tasks, history
  }

  constructor() {
    super()
    this.toggleView = _.throttle(this.toggleView, 1000)
  }

  onPress(task) {
    if (task) {
      this.props.routesActions.push(Routes.TASK_DETAILS, {task})
    }
  }

  renderTask(task) {
    const plysh = _(this.props.plysh).find(p => p.task.id == task.id)

    return (
      <Task reactOnDestroy={true} task={task} plysh={plysh} plyshTime />
    )
  }

  componentWillMount() {
    this.showTutorial = !this.props.settings.finishedTutorial
    InteractionManager.runAfterInteractions(() => {
      this.props.plyshActions.load()
      .then(() => this.props.tasksActions.load())
      .then(() => this.props.cardsActions.load())
      .then(() => this.removeOverdueTask())
      .then(() => this.checkInitialRun())
    })
  }

  componentDidUpdate(props, state) {
    if (
      // changed route to tasks
      (
        this.state.view == 'tasks' &&
        this.props.routes.currentRoute.name == 'tasks' &&
        props.routes.currentRoute.name != 'tasks'
      ) ||
      // changed view
      (
        this.state.view == 'tasks' &&
        this.state.view != state.view
      )
    ) {
      this.removeOverdueTask()
    } else if (
      // if history is empty
      this.state.view == 'history' &&
      this.props.tasks.length > 0 &&
      this.props.plysh.length == 0
    ) {
      this.setState({view: 'tasks'})
    }

    // if (!state.updated || this.state.updated) {
    //   this.setState({updated: true})
    // }
  }

  removeOverdueTask() {
    const task = _(this.collection()).find(t => {
        return t.dateTo.isBefore(moment()) && !t._destroy
    })

    if (task) {
      this.props.tasksActions
        .update(_.extend({}, task, { _destroy: true }))
        .then(() => {
          this.timeout = setTimeout(() => {
            this.removeOverdueTask()
          }, 1000)
        })
    } else {
      this.destroyOverdueTask()
    }
  }

  destroyOverdueTask() {
    const task = _(this.collection()).find(t => t.dateTo.isBefore(moment()))
    if (task) {
      this.props.plyshActions.fail(task).then(() => {this.destroyOverdueTask()})
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  newTask() {
    this.props.routesActions.push(Routes.TASK_FORM)
  }

  checkInitialRun() {
    if (this.props.tasks.length <= 0) {
      this.setState({view: 'history'})
    }
  }

  renderAddButton(collection) {
    return (
      <TouchableOpacity onPress={() => this.newTask()}>
        <View style={tasksStyles.addButtonWrapper}>
          <View style={tasksStyles.addButton}>
            <Icon
              name='fontawesome|plus'
              size={25}
              color='white'
              style={tasksStyles.iconStyle}
              />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  renderMenuButton() {
    return (
      <TouchableOpacity onPress={() => this.setState({sidebarOpen: true})}>
        <Icon
          name='fontawesome|bars'
          size={20}
          color='white'
          style={{width: 50, height: 50, marginTop: -15}}
          />
      </TouchableOpacity>
    )
  }

  shouldShowSwitchViewButton() {
    return _(this.props.tasks || []).filter(x => x.archived).length > 0
  }

  toggleView() {
    const nextView = this.state.view == 'history' ? 'tasks' : 'history'
    this.setState({view: nextView})
  }

  renderSwitchViewButton() {
    if (this.shouldShowSwitchViewButton()) {
      return (
        <TouchableOpacity onPress={() => this.toggleView()}>
          <Icon
            name={this.state.view == 'history' ? 'fontawesome|list' : 'fontawesome|history'}
            size={20}
            color='white'
            style={{width: 50, height: 50, marginTop: -15}}
            />
        </TouchableOpacity>
      )
    } else {
      return <View />
    }
  }

  collection() {
    return _(this.props.tasks)
      .chain()
      .filter(x => !x.archived)
      .sortBy(x => x.dateTo.diff(moment()))
      .value()
  }

  renderTasks(collection) {
    if (collection.length <= 0) {
      return (
        <View style={{ flex: 1, padding: 20, alignItems: 'center', justifyContent: 'center' }}>
          <Text style={[styles.logoText, { fontSize: 30, fontWeight: '200' }]}>
            Hi, {this.props.settings.name}!
          </Text>
          <Text style={[styles.logoText, { marginTop: 20, fontSize: 16, fontWeight: '100' }]}>
            Add your next challenge now.
          </Text>
        </View>
      )
    }

    return (
      <CollectionList
        collection={collection}
        onPress={task => this.onPress(task)}
        renderRow={task => this.renderTask(task)}
        renderFooter={() => <View style={{height: 100}} />}
        />
    )
  }

  renderTab(collection) {
    const history = () => <PlyshHistory extraSpaceAtBottom={true} onlyBody={true} />
    const active = (collection) => this.renderTasks(collection)

    const tab = (this.state.view == 'history') ? history : active

    return (
      <View style={{flex: 1}}>
        {tab(collection)}
      </View>
    )
  }

  render() {
    const title = 'Plysh'

    const collection = this.collection()

    return (
      <Sidebar isOpen={this.state.sidebarOpen} onChange={x => this.setState({sidebarOpen: x})}>
        <Main title={title} leftButton={this.renderMenuButton()} rightButton={this.renderSwitchViewButton()}>
          <View style={[styles.mainBox, tasksStyles.listContainer]}>
            {this.renderTab(collection)}
            {this.renderAddButton(collection)}
          </View>
        </Main>
      </Sidebar>
    )
  }
}
