import React, {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'
import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import orientation from '../../actions/orientation'
import styles from '../../stylesheets/main'
import SideMenu from 'react-native-side-menu'
import NavigationBar from 'react-native-navbar'

import Sidebar from './sidebar'

const buttonStyles = StyleSheet.create({
  main: {
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
});

@connector({routes, orientation})
export default class Main extends React.Component {
  backButtonConfig() {
    return (
      <TouchableOpacity onPress={() => this.props.routesActions.pop()}>
        <Icon
          name='fontawesome|arrow-left'
          size={20}
          color='white'
          style={{width: 50, height: 50, marginTop: -15}}
          />
      </TouchableOpacity>
    )
  }

  leftButtonConfig() {
    return (
      <View style={buttonStyles.main}>
        {(this.props.leftButton) ? (
          this.props.leftButton
        ) : (
          (this.props.routes.allRoutes.length > 1) ? (
            this.backButtonConfig()
          ) : (
            <View />
          )
        )}
      </View>
    )
  }

  rightButtonConfig() {
    return (
      <View style={buttonStyles.main}>
        {this.props.rightButton}
      </View>
    )
  }

  renderBody() {
    return (
      <View style={[styles.bgDefault, styles.mainBox]}>
        <NavigationBar
          tintColor='rgb(37,37,37)'
          statusBar={{
            style: 'light-content',
            hidden: false
          }}
          title={{
            title: (this.props.title || ''),
            tintColor: 'white'
          }}
          leftButton={this.leftButtonConfig()}
          rightButton={this.rightButtonConfig()}
          />
        {this.props.children}
      </View>
    )
  }

  renderMenu() {
    return <Sidebar />
  }

  render() {
    return (
      <View style={{flex: 1}} removeClippedSubviews={true}>
        {this.renderBody()}
      </View>
    )
  }
}

