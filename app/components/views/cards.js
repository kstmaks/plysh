import React, {
  View,
  Text,
  TouchableOpacity,
  InteractionManager
} from 'react-native'

import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import cards from '../../actions/cards'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'

import Routes from '../../lib/routes'
import Main from './main'
import CollectionList from '../forms/collection_list'

@connector({cards, routes})
export default class Cards extends React.Component {
  onPress(card) {
    console.log('here')
    if (this.props.routes.modal) {
      this.props.routesActions.popModal({card})
    } else {
      this.props.routesActions.push(Routes.CARD_DETAILS, {card})
    }
  }

  componentWillMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.cardsActions.load()
    })
  }

  renderCard(card) {
    return (
      <View style={[tasksStyles.detailsRow, styles.cardsRow]}>
        <Icon
          name='fontawesome|credit-card'
          size={20}
          color='#AAA'
          style={{width: 30, height: 20, marginRight: 10}}
          />
        <Text style={styles.cardsRowText}>{card.title} ({card.number})</Text>
        <Icon
          name='fontawesome|chevron-right'
          size={20}
          color='#AAA'
          style={{width: 30, height: 20}}
          />
      </View>
    )
  }

  newCard() {
    this.props.routesActions.push(Routes.CARD_FORM)
  }

  renderAddButton() {
    return (
      <TouchableOpacity onPress={() => this.newCard()}>
        <Icon
          name='fontawesome|plus'
          size={20}
          color='rgb(228,55,98)'
          style={{width: 50, height: 50, marginTop: -15}}
          />
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <Main title="Payment Methods" rightButton={this.renderAddButton()}>
        <View style={[styles.mainBox, styles.bgLight]}>
          <CollectionList
            collection={this.props.cards}
            onPress={card => this.onPress(card)}
            renderRow={card => this.renderCard(card)}
            />
        </View>
      </Main>
    )
  }
}


