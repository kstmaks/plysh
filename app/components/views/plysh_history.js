import React, {
  View,
  Text,
  ListView,
  InteractionManager,
  TouchableOpacity,
  Image,
  NativeModules
} from 'react-native'
import Camera from 'react-native-camera'
import { Icon } from 'react-native-icons'
import _ from 'underscore'
import moment from 'moment'
import ExImage from '../../lib/ex_image'

import {
  LoginManager,
  ShareApi,
  SharePhoto,
  SharePhotoContent,
} from 'react-native-fbsdk'

import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource'

import connector from '../../connector'
import tasks from '../../actions/tasks'
import routes from '../../actions/routes'
import recipients from '../../actions/recipients'
import orientation from '../../actions/orientation'
import plysh from '../../actions/plysh'
import styles from '../../stylesheets/main'
import Routes from '../../lib/routes'
import Config from '../../config'

import Main from './main'
import Money from '../forms/money'
import CollectionList from '../forms/collection_list'
import AssetImage from '../forms/asset_image'

@connector({tasks, plysh, recipients, routes, recipients, orientation})
export default class PlyshHistory extends React.Component {
  state = {
    sharing: false
  }

  componentWillMount() {
    InteractionManager.runAfterInteractions(() => {
      this.props.plyshActions.load()
    })
  }

  history() {
    return _(this.props.plysh).sortBy(x => -x.createdAt.unix())
  }

  repeatTask(task) {
    this.props.routesActions.push(Routes.TASK_FORM, {task})
  }

  getRecipient(id) {
    return _(this.props.recipients).findWhere({id})
  }

  share(plysh) {
    const recipient = _(this.props.recipients).find(x => x.id == plysh.task.recipient)

    const successTitle = `I plyshed "${plysh.task.title}"!`
    const failureTitle = `I failed "${plysh.task.title}" and lost some money, but no worries!`

    const successText = `Check out this app ${Config.linkToApplication}! I just accomPlyshed my task thanks to it.`
    const failureText = `All money were donated to ${recipient.title}. Check out this app ${Config.linkToApplication}`

    const title = plysh.success ? successTitle : failureTitle
    const text = plysh.success ? successText : failureText

    const textContent = `${title}\n${text}`

    this.setState({sharing: true})

    NativeModules.PLYImageManager.temporarySave({
      tag: resolveAssetSource(plysh.image).uri
    }, ({error, path}) => {

      if (error) {
        alert('Can\'t create image!')
        this.setState({sharing: false})
      } else {

        const photo = { imageUrl: path }
        const content = { contentType: 'photo', photos: [photo] }

        ShareApi
          .share(content, '/me', textContent)
          .then(result => {
            alert('Thanks for sharing!')
            this.setState({sharing: false})
          })
          .catch(() => {
            LoginManager
              .logInWithPublishPermissions(['publish_actions'])
              .then(result => {
                if (!result.isCancelled) {
                  this.share(plysh)
                }
              })
              .catch(() => {
                alert('Couldn\'t share')
                this.setState({sharing: false})
              })
          })
      }
    })
  }

  renderPlaceholder() {
    const task = {
    }
    const plysh = {
      success: true
    }
    return (
      <Task task={task} plysh={plysh} />
    )
  }

  renderPlysh(plysh) {
    if (!plysh) {
      if (this.props.tasks.length <= 0) {
        try {
          plysh = {
            success: true,
            image: require('../../images/feedchildren.png'),
            createdAt: moment(),
            stub: true,
            task: {
              recipient: 'feedthechildren',
              title: 'Some title',
              dateTo: moment(),
              image: require('../../images/feedchildren.png'),
              price: {
                amount: 10,
                currency: 'USD'
              }
            }
          }

        } catch (e) {
          alert(e)
        }
      } else {
        return <View style={{height: 100}} />
      }
    }

    const {width, height} = this.props.orientation

    return (
      <View style={styles.card}>
        <View style={styles.cardTitle}>
          <Text style={styles.cardTitleText}>
            {plysh.success ? 'Saved' : 'Gave'}
            {" "}<Money money={plysh.task.price} />{" "}
            {!plysh.success && `to ${this.getRecipient(plysh.task.recipient).title}`}
          </Text>
          <Text style={styles.cardTitleDateText}>{plysh.createdAt.fromNow()}</Text>
        </View>
        <View style={[styles.cardImageWrapper, {position: 'relative'}]}>
          <AssetImage
            resizeMode="contain"
            keepWithin={{width, height: height * 0.7}}
            style={[styles.cardImage, { width: width, height: height * 0.7 }]}
            source={plysh.image}
            />
          {plysh.stub &&
            <Text style={{
              position: 'absolute',
              top: 0,
              left: 0,
              bottom: 0,
              right: 0,
              alighItems: 'center',
              justifyContent: 'center',
              textAlign: 'center',
              verticalAlign: 'middle',
              color: 'white',
              backgroundColor: 'rgba(0,0,0,.7)'
            }}>
              {"\n\n\n\nSet Task.\n\nCommit.\n\nAccomPlysh."}
            </Text>
          }
        </View>
        {!plysh.stub &&
          <View style={[styles.cardDescription, {paddingBottom: 0}]}>
            <View style={styles.cardDescriptionText}>
              <Text>{plysh.task.title}</Text>
            </View>
            <TouchableOpacity onPress={() => this.share(plysh)}>
              <Icon
                name="fontawesome|facebook"
                size={20}
                color="#115599"
                style={{width: 30, height: 25, margin: 10, marginTop: 10, textAlign: 'center' }} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.repeatTask(plysh.task)}>
              <Icon
                name="fontawesome|repeat"
                size={20}
                color="#666"
                style={{width: 30, height: 25, margin: 10, marginTop: 10, textAlign: 'center' }} />
            </TouchableOpacity>
          </View>
        }
        <View style={[styles.cardDescription, {paddingTop: 5}]}>
          {plysh.success ?
            <Text style={styles.cardPlyshed}>
              Plyshed
            </Text> :
            <Text style={styles.cardFailed}>
              Failed
            </Text>}
        </View>

        {this.state.sharing &&
          <View style={{width, height, position: 'absolute', top: 0, left: 0, justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: 'rgba(0,0,0,.8)'}}>
            <Text style={{fontSize: 15, color: 'white'}}>Sharing on facebook...</Text>
          </View>}
      </View>
    )
  }

  renderBody() {
    const collection = this.props.extraSpaceAtBottom ? this.history().concat([null]) : this.history()
    return (
      <CollectionList
        collection={collection}
        renderRow={plysh => this.renderPlysh(plysh)}
        />
    )
  }

  renderWithMain() {
    return (
      <Main title="History">
        {this.renderBody()}
      </Main>
    )
  }

  render() {
    return this.props.onlyBody ? this.renderBody() : this.renderWithMain()
  }
}
