import React, { 
  View, 
  Text,
  ListView,
  TouchableOpacity,
  AlertIOS,
  ActivityIndicatorIOS
} from 'react-native'
import moment from 'moment'

import { Icon } from 'react-native-icons'

import connector from '../../connector'
import routes from '../../actions/routes'
import cards from '../../actions/cards'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'
import Routes from '../../lib/routes'
import Main from './main'
import Money from '../forms/money'
import PrimaryButton from '../forms/primary_button'
import CollectionList from '../forms/collection_list'

@connector({routes, cards})
export default class CardDetails extends React.Component {
  constructor() {
    super()
    this.state = {loading: false}
  }

  getRows() {
    const card = this.props.card
    return [
      {title: 'Title', render: card.title},
      {title: 'Number', render: card.number},
    ]
  }

  renderRow({title, render}) {
    return (
      <View style={tasksStyles.detailsRow}>
        <Text style={tasksStyles.detailsRowText}>{title}</Text>
        <Text style={[tasksStyles.detailsRowText, tasksStyles.detailsRowTextRight]}>{render}</Text>
      </View>
    )
  }

  deleteCard() {
    this.setState({loading: true})
    this.props.cardsActions.remove(this.props.card)
      .then(() => this.setState({loading: false}))
      .then(() => this.props.routesActions.pop())
  }

  showDeleteAlert() {
    if (!this.state.loading) {
      AlertIOS.alert(
        'Are you sure?',
        null,
        [
          { text: 'Yes', onPress: () => this.deleteCard() },
          { text: 'No', style: 'cancel' }
        ],
        'yes-no'
      )
    }
  }

  renderDeleteButton() {
    const button = (
      <TouchableOpacity onPress={() => this.showDeleteAlert()}>
        <Icon 
          name='fontawesome|trash'
          size={20}
          color='rgb(228,55,98)'
          style={{width: 50, height: 50, marginTop: -15}}
          />
      </TouchableOpacity>
    )

    const loading = (
      <ActivityIndicatorIOS
        size="small"
        animating={true}
        style={{flex: 1, marginRight: 15, marginBottom: 2}} />
    )

    return this.state.loading ? loading : button
  }

  render() {
    const card = this.props.card

    return (
      <Main title={card.title} rightButton={this.renderDeleteButton()}>
        <View style={[styles.bgLight, styles.mainBox]}>
          <CollectionList
            collection={this.getRows()}
            renderRow={(row) => this.renderRow(row)}
            />
        </View>
      </Main>
    )
  }
}
