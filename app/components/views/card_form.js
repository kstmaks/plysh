import React, { 
  View, 
  Text,
  ScrollView,
  TextInput,
  ActivityIndicatorIOS
} from 'react-native'
import moment from 'moment'
import _ from 'underscore'

import connector from '../../connector'
import routes from '../../actions/routes'
import cards from '../../actions/cards'
import styles from '../../stylesheets/main'
import tasksStyles from '../../stylesheets/tasks'
import Routes from '../../lib/routes'
import Main from './main'
import Money from '../forms/money'
import PrimaryButton from '../forms/primary_button'
import Form from '../forms/form'

@connector({routes, cards})
export default class CardForm extends React.Component {
  constructor(props) {
    super()
    this.state = {
      card: {},
      loading: false
    }
  }

  getFields() {
    return {
      title: {
        type: 'text',
        placeholder: 'Enter Title',
        validations: {
          presence: true
        }
      },
      number: {
        type: 'text',
        placeholder: 'Card Number',
        readable: true,
        validations: {
          presence: true,
          length: {
            maximum: 20
          }
        }
      },
      cvc: {
        type: 'text',
        placeholder: 'CVC',
        readable: true,
        validations: {
          presence: true,
          length: {
            maximum: 4
          }
        }
      },
      expMonth: {
        type: 'picker',
        options: [{ value: null, label: '---' }].concat(
                  _.range(1, 13)
                    .map(value => 
                      ({ value, label: value < 10 ? `0${value}` : `${value}` }))
                 ),
        label: 'Expiration Month',
        validations: {
          presence: true,
          numericality: {
            greaterThan: 0,
            lessThan: 13
          }
        }
      },
      expYear: {
        type: 'picker',
        options: [{ value: null, label: '---' }].concat(
          _.range(moment().year(), moment().add(20, 'years').year()).map(x => ({
            value: x,
            label: `${x}`
          }))
        ),
        label: 'Expiration Year',
        validations: {
          presence: true,
          numericality: {
            greaterThan: moment().year() - 1,
          }
        }
      },
    }
  }

  save() {
    const valid = this.refs.form.validateFields()
    if (valid) {
      this.commit(this.state.card)
    }
  }

  commit(data) {
    const title = data.title
    const promise = this.props.cardsActions.create(title, data)

    this.setState({loading: true})

    promise.then(({success, error}) => {
      this.setState({loading: false})
      if (success) {
        this.onSuccess()
      } else {
        this.refs.form.setErrors(this._parseStripeError(error))
      }
    }).catch(() => {
      this.setState({loading: false})
    })
  }

  onSuccess() {
    this.props.routesActions.pop()
  }

  _parseStripeError(error) {
    const params = { number: 'number', cvc: 'cvc', exp_year: 'expYear', exp_month: 'expMonth' }
    const param = params[error.param]
    let data = {}
    data[param] = [error.message]
    return data
  }

  render() {
    const card = this.state.card
    const title = (card.title || '').length > 0 ? card.title : 'New card'

    return (
      <Main title={title}>
        <View style={[styles.bgLight, styles.mainBox]}>
          <Form 
            ref="form"
            value={this.state.card}
            onChange={(card) => this.setState({card})}
            fields={this.getFields()} 
            />
        </View>
        <View style={tasksStyles.detailsFooter}>
          <PrimaryButton 
            style={tasksStyles.detailsButton} 
            label='Create Payment Method' 
            loading={this.state.loading}
            onPress={() => this.save()}
            />
        </View>
      </Main>
    )
  }
}
