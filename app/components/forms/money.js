import React, {Text} from 'react-native'

export default class Money extends React.Component {
  static renderCurrency(currency) {
    switch (currency) {
    case 'USD': return ['$', null]
    case 'RUB': return [null, '₽']
    default: return []
    }
  }

  static renderAmount(amount) {
    const strFloat = amount.toFixed(2)
    const strInt = Math.round(amount).toFixed(2)

    if (strFloat == strInt) {
      return amount.toFixed(0)
    } else {
      return strFloat
    }
  }

  static asString({currency, amount}) {
    const currencyRender = this.renderCurrency(currency)
    const amountRender = this.renderAmount(amount)
    return `${currencyRender[0] || ""}${amountRender}${currencyRender[1] || ""}`
  }

  static test() {
    return "!23"
  }

  render() {
    return (
      <Text {...this.props}>{Money.asString(this.props.money)}</Text>
    )
  }
}
