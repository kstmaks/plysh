import React, {
  View,
  Dimensions,
  InteractionManager,
  Text
} from 'react-native'

import ViewPager from 'react-native-viewpager'
import _ from 'underscore'

import getValueLinkMixin from '../../mixins/get_value_link'
import stylesSwiper from '../../stylesheets/swiper'

class DotsTabBar extends React.Component {
  renderDot(active, i) {
    let style = [stylesSwiper.dot]
    if (active) {
      style.push(stylesSwiper.dotActive)
    }
    return <View style={style} key={i} />
  }
  render() {
    return (
      <View style={stylesSwiper.tabBar}>
        {this.props.tabs.map((tab, i) =>
          this.renderDot(i == this.props.activeTab, i))}
      </View>
    )
  }
}

export default class Swiper extends React.Component {
  constructor() {
    super()
    this.ds = new ViewPager.DataSource({pageHasChanged: (p1, p2) => p1 !== p2})
  }

  getDataSource() {
    return this.ds.cloneWithPages(
      _(this.props.children).map((x, i) => i)
    )
  }

  goToPage(page) {
    this.refs.pager.goToPage(page)
  }

  renderTabBar() {
    return <DotsTabBar
      tabs={this.props.tabs}
      activeTab={this.props.page}
      />
  }

  render() {
    return (
      <View style={{flex: 1}}>
        {!this.props.tabsAtBottom &&
          <View>
            {this.renderTabBar()}
          </View>}
        <ViewPager
          ref="pager"
          style={{flex: 1}}
          dataSource={this.getDataSource()}
          renderPage={page => this.props.children[page]}
          locked={true}
          renderPageIndicator={false}
          />
        {this.props.tabsAtBottom &&
          <View>
            {this.renderTabBar()}
          </View>}
      </View>
    )
  }
}
