import React, {
  View,
  Text
} from 'react-native'

import tasksStyles from '../../../stylesheets/tasks'

export default class Field extends React.Component {
  renderErrorDescription(err, idx) {
    return (
      <View key={idx} style={tasksStyles.detailsRowErrorDescription}>
        <Text>{err}</Text>
        <View style={tasksStyles.detailsErrorTriangle} />
      </View>
    )
  }

  render() {
    const errors = this.props.errors || []

    const style = [
      tasksStyles.detailsRow,
      errors.length > 0 && tasksStyles.detailsRowError,
    ]

    return (
      <View>
        <View style={style}>
          {this.props.children}
        </View>
        {errors.map((err, i) => this.renderErrorDescription(err, i))}
      </View>
    )
  }
}
