import React, { 
  View, 
  Text,
  TouchableOpacity,
} from 'react-native'

import styles from '../../../stylesheets/main'
import buttonStyles from '../../../stylesheets/button'

export default class Apply extends React.Component {
  render() {
    return (
      <TouchableOpacity onPress={() => this.props.onPress()}>
        <View style={buttonStyles.applyButton}>
          <Text style={buttonStyles.applyButtonText}>{this.props.label}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}
