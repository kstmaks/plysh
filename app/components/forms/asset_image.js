
import React, { View, Component, Image, ActivityIndicatorIOS } from 'react-native'

import { ReadImageData } from 'NativeModules';

import _ from 'underscore'

class AssetImage extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = { source: null, loading: true, style: {} }
  }

  componentWillMount() {
    const { source } = this.props

    this.shouldUpdatePicture = true

    if (source && source.uri && source.uri.startsWith('assets-library://')) {
      ReadImageData.readImage(source.uri, (imageBase64) => {
        if (this.shouldUpdatePicture) {
          const uri = `data:image/png;base64,${imageBase64}`
          this.setState({ source: { uri } }, () => {
            this.resize(uri)
          })
        }
      })
    } else {
      this.setState({ source, loading: false })
    }
  }

  componentWillUnmount() {
    this.shouldUpdatePicture = false
  }

  resize(uri) {
    const { keepWithin } = this.props

    if (keepWithin) {
      Image.getSize(uri, (width, height) => {
        if (this.shouldUpdatePicture) {
          const ratio = width > 0 ? height/width : 1;

          const dwidth = keepWithin.width
          const dheight = keepWithin.height

          const wwidth = dwidth
          const wheight = wwidth * ratio

          const hheight = dheight
          const hwidth = hheight / ratio

          const style = (wheight > dheight)
            ? { width: hwidth, height: hheight }
            : { width: wwidth, height: wheight }

          this.setState({ loading: false, style })
        }
      });
    } else {
      this.setState({ loading: false })
    }
  }

  render() {
    const { source, loading, style } = this.state

    const stub = () => (
      <View
        {...this.props}
        style={[
          this.props.style,
          { justifyContent: 'center', alignItems: 'center' },
        ]}>
        <ActivityIndicatorIOS size="large" />
      </View>
    )

    const image = () => (
      <Image
        source={source}
        {..._.omit(this.props, 'source', 'style')}
        resizeMode="cover"
        style={[
          this.props.style,
          style
        ]}
        />
    )

    return (source && !loading) ? image() : stub()
  }
}

export default AssetImage;
