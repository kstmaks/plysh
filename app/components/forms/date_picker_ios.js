import { PropTypes } from 'react'
import { DatePickerIOS } from 'react-native'

DatePickerIOS.propTypes.date = PropTypes.any.isRequired
DatePickerIOS.propTypes.onDateChange = PropTypes.func

export default DatePickerIOS
