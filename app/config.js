import _ from 'underscore'

const ENV = 'development'

const configs = {
  default: {
    stripeSecretKey: 'sk_test_KtzyxuX9wQ33lxzBeturvpd3',
    currency: 'usd',
    linkToApplication: "http://apple.com"
  },

  development: {
  },

  production: {
  }
}

export default _.extend({}, configs.default, configs[ENV])
