export default function(component) {
  component.prototype.getValueLink = function(props) {
    let p = props || this.props

    if (p.valueLink != undefined) {
      return p.valueLink
    } else {
      return {
        value: p.value == undefined ? this.__tempValue : p.value,
        requestChange: p.onChange == undefined ? (x => this.__tempValue = x) : p.onChange
      }
    }
  }
  return component
}
